#!/usr/bin/env bash

# Install Required Packages...
if `command -v yaourt >/dev/null 2>&1`; then
	set -x
	yaourt -Syu --needed \
		base base-devel cmake doxygen graphviz cppcheck \
		curl unzip \
		jdk8-openjdk jre8-openjdk
else
	set -x
	apt-get update -qq
	apt-get install -qq >/dev/null \
		build-essential cmake doxygen graphviz cppcheck \
		curl unzip \
		openjdk-8-jdk openjdk-8-jre
fi

# Install Android
ANDROID_HOME="${HOME}/Android/Sdk"
mkdir -p ${ANDROID_HOME} && cd ${ANDROID_HOME}

## Install the NDK in ${ANDROID_HOME}/ndk-bundle like Android Studio do.
NDK="android-ndk-r13b-linux-x86_64.zip"
curl -sLO https://dl.google.com/android/repository/${NDK} && \
	unzip -q ${NDK} && rm ${NDK} && \
	mv android-ndk* ${ANDROID_HOME}/ndk-bundle

## Install the SDK 25.2.3 and API-19 aka Android 4.4(KITKAT)
SDK="tools_r25.2.3-linux.zip"
curl -sLO https://dl.google.com/android/repository/${SDK} && \
	unzip -q ${SDK} && rm ${SDK}
export PATH=${ANDROID_HOME}/tools:$PATH && \
	echo y | android -s update sdk --force --all --no-ui --filter \
platform-tools,tools,build-tools-25.0.2,\
build-tools-19.1.0,android-19,addon-google_apis-google-19,\
sys-img-armeabi-v7a-android-19,sys-img-armeabi-v7a-google_apis-19,\
sys-img-x86-android-19,sys-img-x86-google_apis-19

## Install CMake
#CMAKE="cmake-3.6.3155560-linux-x86_64.zip"
#curl -sLO https://dl.google.com/android/repository/${CMAKE} && \
#	unzip -qd ~/android/android-sdk-linux/cmake ${CMAKE} && rm {CMAKE}

## AVD
echo no | android create avd --name Default --target android-19 --abi armeabi-v7a
echo no | android create avd --name x86 --target android-19 --abi x86
set +x

echo "Please add this to your zshrc..."
if `command -v yaourt >/dev/null 2>&1`; then
	echo export JAVA_HOME=/usr/lib/jvm/default
else
	echo export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
fi
echo export ANDROID_HOME=${ANDROID_HOME}
echo export ANDROID_NDK=\${ANDROID_HOME}/ndk-bundle
echo export PATH=\
\${ANDROID_HOME}/tools:\
\${ANDROID_HOME}/platform-tools:\
\${ANDROID_HOME}/build-tools/19.1.0:\
\$PATH

