# Dockerfile for C++/CMake/Android Development Environment

From [ubuntu:devel](https://hub.docker.com/_/ubuntu/):
* [build-essential](http://packages.ubuntu.com/zesty/build-essential)
* [cmake](http://packages.ubuntu.com/zesty/cmake)
* [doxygen](http://packages.ubuntu.com/zesty/doxygen)
* [cppcheck](http://packages.ubuntu.com/zesty/cppcheck)
* [curl](http://packages.ubuntu.com/zesty/curl)
* [unzip](http://packages.ubuntu.com/zesty/unzip)
* OpenJDK-8 ([JDK](http://packages.ubuntu.com/zesty/openjdk-8-jdk) & [JRE](http://packages.ubuntu.com/zesty/openjdk-8-jre))
* [Android NDK r13b](https://developer.android.com/ndk/downloads/index.html)
* [Android SDK r25.2.3](https://developer.android.com/studio/index.html#downloads) API-19 Full (build-tools-19.1.0,tools,platform-tools,android-19 aka 4.4.2)

with ENV:  
* JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
* ANDROID_HOME=/root/Android/Sdk
* ANDROID_NDK=${ANDROID_HOME}/ndk-bundle
* PATH=${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/build-tools/19.1.0:$PATH

and AVD:
```sh
android list avd
Available Android Virtual Devices:
    Name: Default
    Path: /root/.android/avd/Default.avd
  Target: Android 4.4.2 (API level 19)
 Tag/ABI: default/armeabi-v7a
    Skin: WVGA800
---------
    Name: x86
    Path: /root/.android/avd/x86.avd
  Target: Android 4.4.2 (API level 19)
 Tag/ABI: default/x86
    Skin: WVGA800
```

## Docker Build
```sh
docker build -t mizux/android .
```

## Run
```sh
docker run -it mizux/android bash
```

## Publish
```sh
docker push mizux/android:latest
```
