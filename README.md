# Install Android
First you will need to install Android [SDK](https://developer.android.com/studio/index.html#downloads) & [NDK](hhttps://developer.android.com/ndk/downloads/index.html) on your machine.  
Look at [android_install.sh](Docker/android_install.sh) for install script  
or Docker image: [mizux/android](https://hub.docker.com/r/mizux/android) src: [Dockerfile](Docker/Dockerfile)

Last gradle version: [2.14.1 and 3.0](https://gradle.org/gradle-download/)  
Last gradle plugin: [2.1.3](https://developer.android.com/studio/releases/gradle-plugin.html#revisions) 

## Install Android Studio
```sh
wget https://dl.google.com/dl/android/studio/ide-zips/2.2.0.12/android-studio-ide-145.3276617-linux.zip
unzip -d ~/android android-studio-ide-145.3276617-linux.zip
rm android-studio*.zip
export PATH=~/android/android-studio/bin:$PATH
```
Then you can run android studio using:
```sh
studio.sh
```

# Running GitLab job locally
First you will need to install:
```sh
yaourt -S docker gitlab-ci-multi-runner
```
Note: by default docker store its ~100Go file in */var/lib/docker/* but you can modify it by editing */etc/systemd/system/multi-user.target.wants/docker.service* if docker.service is not running.  
e.g. for relocation to */home/docker* replace by:
```sh
ExecStart=/usr/bin/dockerd -g /home/docker -H fd://
```
(need to be root , and create the directory as root before ;))

Then just restart the daemon:
```sh
systemctl daemon-reload && systemctl start docker
```
And enable it a startup:
```sh
systemctl enable docker
```

Now in your repository you can run gitlab-CI jobs.  
e.g. for building "build:linux":
```sh
gitlab-ci-multi-runner exec docker build:linux
```
Note: According to the doc it is currently impossible to use artifact with docker
ToDo: test  with “shell” executor...

# Create Android Project (Gradle Way)
Once everything installed, it's time to create our first hello world.
Android provide a out of date cli tools for that !
```sh
android create project --name Plop --target android-19 --package net.mizux.plop --activity MainActivity --gradle -v 2.1.3 -p .
```

If you try to build using *./gradlew* you'll get errors since gradle 1.12 is
too old to work with android plugin 2.1.3...  
To Fix: simply edit [gradle/wrapper/gradle-wrapper.properties](updated-gradle/gradle/wrapper/gradle-wrapper.properties) 
and use "gradle-2.14.1-all.zip" instead of 1.12 one

## Gradlew
To Manage gradlew version and update it:
```sh
$ ./gradlew wrapper --gradle-version 2.14.1
```
Updated files should be:
```
.
+- gradle
|  +- wrapper
|     +- gradle-wrapper.jar
|     +- gradle-wrapper.properties
+- gradlew
+- gradlew.bat
```

## Proguard
Also project will fail since recent android plugin didn't use proguard anymore
but minified cf [build.gradle](updated-gradle/build.gradle) file 
so replace the line by "minifyEnabled false".

# Using NDK Video
NDK provide OpenGL ES access

# Using NDK Audio
Accessing audio is done through [OpenSL ES (Open Sound Library for Embedded Systems)](https://developer.android.com/ndk/guides/audio/getting-started.html)  
You can find a sample
[here](https://github.com/googlesamples/android-ndk/tree/android-mk/native-audio)

# Misc
## CMake & QT5
By using *androiddeployqt* qt5 will generate dummy android project then using readelf recursively, loads all qt5 library the target require.
```sh
.
+- assets
|  +- --Added-by-androiddeployqt--
+- libs
|  +- armeabi-v7a // need archi name !
|     +- libtarget.so
|     +- libQt5widget.so …
+- gradle
|  +- wrapper
|     +- gradle-wrapper.jar
|     +- gradle-wrapper.properties
+- gradlew
+- gradlew.bat
```

## Blogs
Blog: http://flohofwoe.blogspot.fr/2014/04/cmake-and-android-ndk.html
[CMake fips android Toolchain](https://github.com/floooh/fips/blob/master/cmake-toolchains/android.toolchain.cmake)

armeabi-v7a application will run on Cortex A# devices like Cortex A8, A9, and A15. It supports multi-core processors and it supports -mfloat-abi=hard. So, if you build your application using -mfloat-abi=hard, many of your function calls will be faster.

[Build App using Makefile](http://androidappdevlopment.blogspot.fr/2012/05/android-application-build-using.html)

[Build App from cli](http://geosoft.no/development/android.html)

android-native-app-glue.h  
http://stackoverflow.com/questions/26499726/gradle-unable-to-find-android-native-app-glue

